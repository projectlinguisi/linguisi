<?php
require_once "../config.php";
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }

				.odp{
					display: inline-block;
					margin: 0, 5px, 0, 5px;
				}


				.odp:hover{
					background-color: yellow;
					cursor: pointer;

				}

    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('../bgs/linguisi_background_dark.jpg');">
	<header>
		<nav class="navbar navbar-expand-md navbar-dark bg-success">
			<div class="container-fluid">
				<nav class="navbar">
					<a class="m-0" href="/linguisi/index.php"><img src="logo.png" width="200" alt=""></a>
				</nav>
					 <h1 class='text-white'>Quiz</h1>
				<!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
				<div class="navbar">
					<ul class="navbar-nav">
						<li ><a class="btn btn-danger" href="/linguisi/index.php?logout='1'"> logout </a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<br>


    <div class="content w-100 p-3 text-white" style="width: 100%; margin: 0px auto; background-color: #153D30;">
      <h2><center><br>
		<?php
			if(isset($_POST)){
				$nquestions = $_POST["nquestions"];
				$npoints = 0;
				//echo $nquestions;
				for($nr=0;$nr<$nquestions;$nr++){
					@$answer = $_POST["odpowiedz$nr"];
					@$correctAnswer = $_POST["poprawnaOdpowiedz$nr"];
					if($answer == $correctAnswer){++$npoints;}
				}
				echo "WYNIK: ".$npoints."/".$nquestions;

        $sql = "INSERT INTO stats (userid, username, game,points, activitydate)
        VALUES(?,?,?,?,?);";
        $date = date("Y-m-d H:i:s");
        $gameid=2;
        if ($stmt = $link->prepare($sql)) {
            $stmt->bind_param("isiis", $_SESSION["globaluserid"],$_SESSION['username'],$gameid,$npoints,$date);
            if ($stmt->execute()) {
            } else {
                echo "Error! Please try again later.";
                exit();
            }
            $stmt->close();
        }
        $tmpuserid=$_SESSION["globaluserid"];

        $sql2 = "SELECT * FROM rankings where iduser=$tmpuserid";
        // echo "SELECT * FROM rankings...<br>";
        if ($stmt2 = $link->prepare($sql2)) {

            if ($stmt2->execute()) {
                $result2 = $stmt2->get_result();
                if ($result2->num_rows >0){
                  // echo "jest<br>";
                  $sqlupdate = "UPDATE rankings SET points = points + ? WHERE iduser=?";
                  if ($stmt3 = $link->prepare($sqlupdate)) {
                      $stmt3->bind_param("ii",$npoints, $_SESSION["globaluserid"]);
                      if ($stmt3->execute()) {
                      } else {
                          echo "Error! Please try again later.";
                          exit();
                      }
                      $stmt3->close();
                  }
                }else{
                  // echo "niema<br>";
                  $sqlinsert = "INSERT INTO rankings (iduser, username, points)  VALUES(?,?,?);";
                  if ($stmt3 = $link->prepare($sqlinsert)) {
                      $stmt3->bind_param("isi", $_SESSION["globaluserid"],$_SESSION['username'],$npoints);
                      if ($stmt3->execute()) {
                      } else {
                          echo "Error! Please try again later.";
                          exit();
                      }
                      $stmt3->close();
                  }
                }
            } else {
                echo "stms2 error<br>";
            }
            $stmt2->close();
        }else {
            echo "stms2 prepare error<br>";
        }

			}
	    ?>
    </h2>
    <br>
    </div><br><br><br><br>
    <div class="content w-75 p-3 text-white" style="width: 100%; margin: 0px auto;">
    <a href="../games.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Back to games menu</a><br><br>
    <a href="index.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Play Again</a><br><br>
    </div>




</body>
</html>
