class Quiz{
	
	constructor(question, correctAnswer, answers){
		this.question = question
		this.correctAnswer = correctAnswer
		this.answers = answers
	}
	
	shuffleAnswers(){
		let temp = new Array(this.answers.length)
		let i = 0
		while(temp.includes(undefined)){
			let rnd = Math.floor(Math.random() * Math.floor(this.answers.length))
			if(temp[rnd] == undefined){
				temp[rnd] = this.answers[i++]
			}
		}
		this.answers = temp
	}
}