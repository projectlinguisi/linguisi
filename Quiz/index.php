<?php


  session_start();

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }

  if(!(isset($_SESSION["choosen_game"]) && isset($_SESSION["selected_list_id"]) && isset($_SESSION['nquestions']))){
	  header("location: ../games.php");
  }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }

				.odp{
					display: inline-block;
					margin: 0, 5px, 0, 5px;
				}


				.odp:hover{
					background-color: yellow;
					cursor: pointer;

				}

    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('../bgs/linguisi_background_dark.jpg');" onload="losuj()">
	<header>
		<nav class="navbar navbar-expand-md navbar-dark bg-success">
			<div class="container-fluid">
				<nav class="navbar">
					<a class="m-0" href="/linguisi/index.php"><img src="logo.png" width="200" alt=""></a>
				</nav>
					 <h1 class='text-white'>Quiz</h1>
				<!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
				<div class="navbar">
					<ul class="navbar-nav">
						<li ><a class="btn btn-danger" href="/linguisi/index.php?logout='1'"> logout </a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<br>

	<?php
$mysqli = @new mysqli('localhost:3307', 'root', '', 'linguisidb');
		if(!$mysqli->errno){


			$selected_list_id = $_SESSION['selected_list_id'];
			$nquestions = $_SESSION['nquestions'];
			$t=[];
			$w=[];
			$i=0;
			$res = $mysqli->query("SELECT * FROM globallist WHERE listid=$selected_list_id");

			while($row = $res->fetch_assoc()){
				$w[$i] = $row["word"];
				$t[$i] = $row["translation"];
				$i++;
			}
			$res->close();
			$mysqli->close();


		}else {die;}

	?>
	<div class="text-white w-50 p-3" style='margin: 0px auto 0px; background-color: #2952a3;'><br>
		<h5 class='text-white'>

			<br>

				<form id="0" class="radioform" action="summary.php" method="post">
					<input type="hidden" name="nquestions" value=<?=$nquestions?>></input>
					<?php
						for($nr=0;$nr<$nquestions;$nr++){
							$style = $nr==$nquestions-1 ? "" : "border-bottom: 1px solid #333333;";
							echo"
								<div style='$style'>
								<div class='question question$nr'></div>

								<input class='odp' type='radio' name='odpowiedz$nr'/>
								<label class='odplabel$nr'></label><br>

								<input class='odp' type='radio' name='odpowiedz$nr'/>
								<label class='odplabel$nr'></label><br>

								<input class='odp' type='radio' name='odpowiedz$nr'/>
								<label class='odplabel$nr'></label><br>

								<input class='odp' type='radio' name='odpowiedz$nr'/>
								<label class='odplabel$nr'></label><br>

								<input class='' type='hidden' id='poprawnaOdpowiedz$nr' name='poprawnaOdpowiedz$nr'></input>
								</div>
                <br>
							";
						}

					?>
				</form>


		</h5>
		<button class="btn btn-danger" onclick="submitAll()">Zatwierdz Wszystko</button>
    </div>

	<script src="Quiz.js"></script>
	<script>
		function submitAll(){
			document.querySelector('.radioform').submit()
		}

		function uncheckRadios(){
			document.querySelectorAll(".odp").forEach(o=>o.checked=false)
		}


		function getRandomInt(max) {
			return Math.floor(Math.random() * Math.floor(max));
		}

		function randomWord(tab){
			curr=0;
			for(index=getRandomInt(tab.length);curr!=index;curr++);
			return tab[curr];
		}

		function createQuiz(w, t){
			question = randomWord(w)
			correctAnswer = t[w.indexOf(question)]

			var answers = new Array(correctAnswer)

			for(i=1;i<4;i++){
				do{
					rw=randomWord(t)
				}while(answers.includes(rw));
				answers.push(rw)
			}

			return new Quiz(question,correctAnswer,answers)
		}

		function losuj(){
			uncheckRadios()
			var w = <?php echo json_encode($w);?>;
			var t = <?php echo json_encode($t);?>;

			var form=document.querySelector(".radioform")
			nquestions=<?php echo json_encode($nquestions);?>;
			for(nr=0,quizes=new Array();nr<nquestions;nr++){
				do{
					quiz = createQuiz(w,t)
				}
				while(quizes.some(q=>q.correctAnswer===quiz.correctAnswer));
				quizes.push(quiz)
				console.log(quizes)

				pytanie = quiz.question
				poprawnaOdpowiedz = quiz.correctAnswer
				quiz.shuffleAnswers()
				arr = quiz.answers

				form.querySelector(`.question${nr}`).innerHTML = pytanie

				i=0
				form.querySelectorAll(`[name=odpowiedz${nr}]`).forEach(o=>o.value=arr[i++])
				i=0
				form.querySelectorAll(`.odplabel${nr}`).forEach(o=>o.innerHTML=arr[i++])
				form.querySelector(`[name=poprawnaOdpowiedz${nr}]`).value=poprawnaOdpowiedz
			}


		}
	</script>


</body>
</html>
