<?php
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}

if (isset($_SESSION['username'])){
  require_once "config.php";
  $sqlid = "SELECT * FROM users";
  // $resultname = $link->query($sqlid);
  // $userid=0;
  //
  // if ($resultname->num_rows > 0){
  //   while($row1 = $resultname->fetch_assoc()) {
  //     if($row1['username']==$_SESSION['username'])
  //      $userid=$row1["id"];
  //      break;
  //   }
  // }
  $userid=$_SESSION['globaluserid'];
  $sql = "SELECT * FROM stats WHERE userid=$userid";

  $result = $link->query($sql);
  $result2= $link->query($sql);
  //
  // $n=$_SESSION['username'];
  // $sql = "SELECT * FROM stats where username=$n";
  //
  // $result = $link->query($sql);
}else {
    	header('location: login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>Statistics</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br><center>
  <div class='w-75 p-3 text-white'>
  <?php
  if ($result->num_rows > 0) {
        echo "<br>";
        echo "<br>";
        echo "<br>";

        echo "<h4>QUIZ</h4>";
        echo "<table border='1' class='table table-striped table-dark'>";
        echo "<thead>";
        echo "<tr class='text-white'>";
        echo "<th>Day</th>";
        echo "<th>Points</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        while ($row = $result2->fetch_assoc()) {
          if($row['game']==2){
            echo "<tr class='text-white'>";
            if(is_null($row['activitydate'])){
              echo "<td>-</td>";
            }else{
            echo "<td>" . $row['activitydate'] . "</td>";
            }
            echo "<td>" . $row['points'] . "</td>";
            echo "</tr>";
          }
        }
        echo "</tbody>";
        echo "</table>";

        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<h4>Words translation</h4>";
        echo "<table border='1' class='table table-striped table-dark'>";
        echo "<thead>";
        echo "<tr class='text-white'>";
        echo "<th>Day</th>";
        echo "<th>Points</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        while ($row = $result->fetch_assoc()) {
            if($row['game']==1){
              echo "<tr class='text-white'>";
              if(is_null($row['activitydate'])){
                echo "<td>-</td>";
              }else{
              echo "<td>" . $row['activitydate'] . "</td>";
              }
              echo "<td>" . $row['points'] . "</td>";
              echo "</tr>";
            }
        }
        echo "</tbody>";
        echo "</table>";





        $result->free();
      } else {



        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<h4>QUIZ</h4>";
        echo "<table border='1' class='table table-striped table-dark'>";
        echo "<thead>";
        echo "<tr class='text-white'>";
        echo "<th>Day</th>";
        echo "<th>Points</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        echo "</tbody>";
        echo "</table>";

        echo "<br>";
        echo "<br>";
        echo "<br>";
        echo "<h4>Words translation</h4>";
        echo "<table border='1' class='table table-striped table-dark'>";
        echo "<thead>";
        echo "<tr class='text-white'>";
        echo "<th>Day</th>";
        echo "<th>Points</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        echo "</tbody>";
        echo "</table>";

        $result->free();
      }
  $link->close();
  ?>
</div>

</body>
</html>
