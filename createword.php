<?php
require_once "config.php";
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
  if (isset($_POST['word'])&&isset($_POST['translation'])) {
      $intnull = null;

      $sql = "INSERT INTO globallist (id,listid, word, translation) VALUES (?,?,?,?)";

      if ($stmt = $link->prepare($sql)) {
          $stmt->bind_param("iiss", $intnull, $_GET['idlist'], $_POST['word'], $_POST['translation']);
          if ($stmt->execute()) {
              header("location: read.php?idlist=" . $_GET["idlist"] . "");
              exit();
          } else {
              echo "Error! Please try again later.";
          }
          $stmt->close();
      }
  }
}else {
    header("location: login.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="#"><img src="logo.png" width="200" alt=""></a>
      </nav>
      <h1 class='text-white'>Add word</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
<br>
<div class='text-white w-75 p-3' style="width: 40%; margin: 0px auto;">
         <h2>Add word</h2>
<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
      <div>
          <label>Word</label>
          <input type="text" name="word" class="form-control" required>
      </div>
      <div>
          <label>Translation</label>
          <input type="text" name="translation" class="form-control" required>
      </div>
      <input type="submit" class="btn btn-primary" value="Submit">
       <a href="userlists.php" class="btn btn-default">Cancel</a>
</form>
</div>
</body>
</html>
