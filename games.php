<?php
  session_start();

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }


  unset($_SESSION["choosen_game"]);
  unset($_SESSION["selected_list_id"]);
  unset($_SESSION['nquestions']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body onload = "unsetSelects()" class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>Quiz</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br>


  <div class='w-75 p-3' style='width: 100%; margin: 0px auto;'>



  <form style = "width: 100%;" method = "post" action = "redirect_to_game.php">

  <?php
	if(isset($_SESSION['err'])){
		echo "<div style = 'width: 100%;' class='error'>";
			echo $_SESSION['err'];
		echo '</div>';
		unset($_SESSION['err']);
	}
   ?>

	<!--<label class="p-1 mb-2 bg-primary text-white">List Selector:</label>-->
	<select id = "num_of_questions_select" class="dropdown bg-primary text-white" name = "nquestions" style = "padding: 1%;margin: 10px 0 5px 0;width:100%">

		<option class = "text-secondary" value="" disabled selected>Number of questions choice</option>
		<option class = 'text-center' value = 3>3</option>
		<option class = 'text-center' value = 5>5</option>
		<option class = 'text-center' value = 10>10</option>

	</select>

   <!--<label class="p-1 mb-2 bg-primary text-white">List Selector:</label>-->
	<select id = "select_list" class="dropdown bg-primary text-white" name = "selected_list_id" style = "padding: 1%;margin: 10px 0 5px 0;width:100%">

		<option class = "text-secondary" value="" disabled selected>List choice</option>
		<optgroup label = "Global lists">
			<?php
				$db = mysqli_connect('localhost:3307', 'root', '', 'linguisidb');
				if($db->errno){die('Couldnt connect to database');}
				//admina
				$res = $db->query("select* from listlist where userid = 1");
				while($row = $res->fetch_assoc()){
					$list_id = $row["idlist"];
					$listname = $row["listname"];
					echo "<option class = 'text-center' value = '$list_id'>$listname</option>";
				}
				$res->close();
			?>
		</optgroup>

		<optgroup label = "My lists">
			<?php
				$my_id = $_SESSION['globaluserid'];
				$res = $db->query("select* from listlist where userid = $my_id");
				while($row = $res->fetch_assoc()){
					$list_id = $row["idlist"];
					$listname = $row["listname"];
					echo "<option class = 'text-center' value = '$list_id'>$listname</option>";
				}
				$res->close();
				$db->close();

			?>
		</optgroup>
	</select>


	<input name = "choosen_game" type = "submit" value="Quiz ABCD" class='btn btn-primary' style="width: 100%; padding: 8%;
	margin: 5px 0 5px 0;"></input>


  </form>
  </div>


<!--
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <h1 class='text-white'>Games</h1>
  <a href='game1/index.php' class='btn btn-primary' style="width: 100%; padding: 8%;">Word translation</a><br><br>
  <!-- <a href='game2/index.php' class='btn btn-primary' style="width: 100%; padding: 8%;">Quiz ABCD</a><br><br>
  <a href='Quiz/index.php' class='btn btn-primary' style="width: 100%; padding: 8%;">Quiz ABCD</a><br><br>
  <a href='game3.php' class='btn btn-primary' style="width: 100%; padding: 8%;">Game3</a><br><br>
  <a href='game4.php' class='btn btn-primary' style="width: 100%; padding: 8%;">Game4</a><br><br>
  </div>;
	-->
  <script>
	function unsetSelects(){
		document.querySelectorAll("select").forEach(s=>s.selectedIndex = 0)
	}
  </script>
</body>
</html>
