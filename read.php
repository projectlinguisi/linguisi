<?php

require_once "config.php";
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
  if (isset($_GET["idlist"]) && !empty(trim($_GET["idlist"]))) {
      $sql = "SELECT * FROM listlist WHERE idlist = ?";
      $val=$_GET["idlist"];
      $sql2 = "SELECT * FROM globallist WHERE listid = '$val'";

      $result2 = $link->query($sql2);

      if ($stmt = $link->prepare($sql)) {
          $stmt->bind_param("i", $_GET["idlist"]);

          if ($stmt->execute()) {
              $result = $stmt->get_result();

              if ($result->num_rows == 1) {
                  $row = $result->fetch_array(MYSQLI_ASSOC);

                  $idlist = $row["idlist"];
                  $isadminlist = $row["userid"];
                  $listname = $row["listname"];
              } else {
                  echo "Error! Please try again later.1";
                  exit();
              }

          } else {
              echo "Error! Please try again later.2";
              exit();
          }
      }
      $stmt->close();
      // $link->close();
  } else {
      echo "Error! Please try again later.3";
      exit();
  }
}else {
    	header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
      </nav>
      <h1 class='text-white'>List view</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
<br>
<div class="w-75 p-3 p-3 mb-2 bg-dark text-light"  style="width: 40%; margin: 0px auto;">
  <center>
    <h2>Current list</h2>
    <h3>
      ID: <?php echo $idlist; ?>
      Name: <?php echo $listname; ?>
    </h3>
  </center>
</div>
<br>
<div class="container-fluid ">
  <?php
  if ($result2->num_rows > 0) {

          echo "<div class='w-75 p-3' style='width: 40%; margin: 0px auto;'>";
          // echo "<div class='row justify-content-center'>";
          // echo "<div class='col-auto'>";
          echo "<p>";
          if($isadminlist==$_SESSION['globaluserid']){
            echo "<a href='createword.php?idlist=$val' class='btn btn-primary'>Add word</a>";
          }
          echo "</p>";

          echo "<table border='1' class='table table-striped table-dark'>";
          echo "<thead>";
          echo "<tr class='text-white'>";
          echo "<th>word</th>";
          echo "<th>translation</th>";
          echo "</tr>";
          echo "</thead>";
          echo "<tbody>";
          while ($row = $result2->fetch_assoc()) {
              echo "<tr class='text-white'>";
              echo "<td>" . $row['word'] . "</td>";
              echo "<td>" . $row['translation'] . "</td>";
              echo "<td>";
              if($isadminlist==$_SESSION['globaluserid']){
                echo "<a href='updateword.php?id=" . $row['id'] . "&idlist=" . $row['listid'] . "' class='btn btn-info'>Update</a>";
                echo "<a href='deleteword.php?id=" . $row['id'] . "&idlist=" . $row['listid'] . "' class='btn btn-danger'>Delete</a>";
              }
              echo "</td>";
              echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";
          // echo "</div>";
          // echo "</div>";
          echo "</div>";
          $result2->free();
      } else {
        echo "<div class='w-75 p-3' style='width: 40%; margin: 0px auto;'>";
        echo "<p>";
        // echo "<p> <a href='index.php?logout='1'' style='color: red;'>logout</a> </p>";
        if($isadminlist==$_SESSION['globaluserid']){
          echo "<a href='createword.php?idlist=$val' class='btn btn-primary'>Add word</a>";
        }

        echo "</p>";
        echo "<p class='lead'><em>No records were found.</em></p>";
        echo "</div>";
      }
  $link->close();
  ?>
</div>
</body>
</html>
