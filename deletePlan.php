<?php
session_start();
if (isset($_SESSION['username'])){
  if (isset($_GET["id"]) && !empty($_GET["id"])) {

      require_once "config.php";
      $sql = "DELETE FROM plans WHERE id = ?";

      if ($stmt = $link->prepare($sql)) {
          $stmt->bind_param("i", $_GET["id"]);
          if ($stmt->execute()) {
              header("location: learningplan.php");
              exit();
          } else {
              echo "Error! Please try again later1.";
          }
      }
      $stmt->close();
      $link->close();
  } else {
      echo "Error! Please try again later2.";
  }
}else {
    	header('location: login.php');
}
?>
