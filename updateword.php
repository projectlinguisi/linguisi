<?php
require_once "config.php";
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
  if (isset($_GET['id'])) {
      $sql = "SELECT * FROM globallist WHERE id = ?";
      if ($stmt = $link->prepare($sql)) {
          $stmt->bind_param("i", $_GET["id"]);
          if ($stmt->execute()) {
              $result = $stmt->get_result();
              if ($result->num_rows == 1) {
                  $row = $result->fetch_array(MYSQLI_ASSOC);

                  $param_word = $row["word"];
                  $param_translation = $row["translation"];
              } else {
                  echo "Error! Data Not Found1";
                  exit();
              }
          } else {
              echo "Error! Please try again later.";
              exit();
          }
          $stmt->close();
      }
  } else {
    header("location: read.php?idlist=" . $_GET["idlist"] . "");
    exit();
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (!empty($_POST["word"])&&!empty($_POST["translation"])) {

          $sql = "UPDATE globallist SET word = ?, translation = ? WHERE id = ?";
          if ($stmt = $link->prepare($sql)) {

              $stmt->bind_param("ssi", $_POST["word"], $_POST["translation"], $_GET["id"]);
              $stmt->execute();
              if ($stmt->error) {
                  header("location: error4.php");
                  exit();
              } else {
                  header("location: read.php?idlist=" . $_GET["idlist"] . "");
                  exit();
              }
              $stmt->close();
          }
      }
      $link->close();
  }
}else {
    header("location: read.php?idlist=" . $_GET["idlist"] . "");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
           <!-- <center><h1>Update word</h1></center> -->
      </nav>
      <h1 class='text-white'>Update word</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
<br>
<div class="w-75 p-3 p-3 mb-2 bg-dark text-white" style="width: 30%; margin: 0px auto;">
   <h2>Update word</h2>
<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
   <div class="form-group">
       <label>Word</label>
       <input type="text" name="word" class="form-control" required value="<?php echo $param_word; ?>">
   </div>
   <div class="form-group">
       <label>Translation</label>
       <input type="text" name="translation" class="form-control" required value="<?php echo $param_translation; ?>">
   </div>
   <input type="submit" class="btn btn-primary" value="Submit">
   <!-- <a href="read.php?idlist=" . $_GET["idlist"] . "" class="btn btn-primary">Cancel</a> -->
</form>
</div>

</body>
</html>
