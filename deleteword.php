<?php
session_start();
if (isset($_SESSION['username'])){
  if (isset($_GET["id"]) && !empty($_GET["id"])) {

      require_once "config.php";
      $sql = "DELETE FROM globallist WHERE id = ?";

      if ($stmt = $link->prepare($sql)) {
          $stmt->bind_param("i", $_GET["id"]);
          if ($stmt->execute()) {
              echo $_GET["idlist"];
              header("location: read.php?idlist=" . $_GET["idlist"] . "");

              exit();
          } else {
              echo "Error! Please try again later.";
          }
      }
      $stmt->close();
      $link->close();
  } else {
      echo "Error! Please try again later.";
  }
}else {
    	header('location: login.php');
}
?>
