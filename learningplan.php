<?php
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
  require_once "config.php";
  $tmp=$_SESSION['globaluserid'];
  $sql = "SELECT * FROM plans WHERE userid=$tmp";

  $result = $link->query($sql);
}else {
    	header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>Learning plans</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br>

  <?php
  if ($result->num_rows > 0) {
          echo "<br>";
          echo "<div class='w-75 p-3' style='width: 60%; margin: 0px auto;'>";
          echo "<h1 class='text-white'>Plans</h1>";
          echo "<a href='addplan.php' class='btn btn-primary'>Add plan</a>";
          echo "<br>";
          echo "<br><center>";
          echo "<table border='1' class='table table-striped table-dark'>";
          echo "<thead>";
          echo "<tr class='text-white'>";

          echo "<th><h3><center>Program name</h3></th>";
          echo "<th><h3><center>Start</h3></th>";
          echo "<th><h3><center>End</h3></th>";
          echo "</tr>";
          echo "</thead>";
          echo "<tbody>";
          while ($row = $result->fetch_assoc()) {

              echo "<tr class='text-white'>";

              echo "<td><h5><center>" . $row['listname'] . "</h4></td>";
              echo "<td><h5><center>" . $row['startDate'] . "</h4></td>";
              echo "<td><h5><center>" . $row['endDate'] . "</h4></td>";
                // echo "<a href='update.php?idlist=" . $row['idlist'] . "' class='btn btn-info'>Update</a>";
              echo "<td><a href='deletePlan.php?id=" . $row['id'] . "' class='btn btn-danger'>Delete</a></td>";
              echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";

          echo "</div>";
          $result->free();
      } else {
          // echo "<a href='index.php' class='btn btn-primary'>Back</a>";
          echo "<br>";
          echo "<div class='w-75 p-3' style='width: 40%; margin: 0px auto;'>";
          echo "<h1 class='text-white'>Plans</h1>";
          echo "<a href='addplan.php' class='btn btn-primary'>Add plan</a>";
          echo "<p class='lead text-white'><em>No records were found.</em></p>";
          echo "</div>";
      }
  $link->close();
  ?>

</body>
</html>
