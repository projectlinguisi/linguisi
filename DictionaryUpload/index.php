<?php
	require_once('validate_file.php');
	require_once('dbconnection.php');
	require_once('insert_new_list.php');
	require_once('insert_words.php');
	require_once('str_ends_with.php');
	require_once "../config.php";
	const MIN_RECORDS = 3;
	const MAX_SIZE = 1024;

?>

<?php
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('../bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <!-- <button type="submit"><img src="logo.png" width="200" alt=""></button> -->
        <!-- <input type="submit" name="submit" style="background: url(logo.png); width:100px; height:25px;" href="index.php"/> -->
        <a class="m-0" href="index.php"><img src="../logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>Add list from file</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br>
  	<div class="content w-75 p-3 text-white" style="width: 100%; margin: 0px auto; background-color: #2952a3;">
  Add list from file



	<form action="" method="post" enctype="multipart/form-data">
		<label for="file">List name:</label>
		<input type="text" name="listname" id="listname" >
		<br>
		<label for="file">File:</label>
		<input type="file" name="plik" id="file">
		<br>
		<input type="submit" name="submit" value="Submit">
	</form>

<?php
	$is_form_submitted = isset($_POST['submit']);
	if ($is_form_submitted) {
		$listname = $_POST['listname'];
		$err_msg = "";
		if ($_FILES['plik']['size'] > MAX_SIZE){
			$err_msg = $err_msg.'Błąd! Plik jest za duży<br>';
		}
		if(empty($listname)){
			$err_msg = $err_msg.'Błąd! Proszę o nazwanie listy';
		}
		echo $err_msg;
		if(empty($err_msg)) {
			$filename = 'input/'.$_FILES['plik']['name'];

			if(str_ends_with($filename,'.txt')){

				move_uploaded_file($_FILES['plik']['tmp_name'],
						$_SERVER['DOCUMENT_ROOT'].'/linguisi/DictionaryUpload/'.$filename);

				@$fp = fopen($filename,'r');
				if($fp){

					$msg = validate_file($fp, MIN_RECORDS);

					/*
					1.sprawdic czy plik jest poprawny
					2.insert listy
					3.insert slow
					4.close(mysqli)
					*/
					// $sqlid = "SELECT * FROM users";
					// $resultname = $link->query($sqlid);
					// $userid=0;
					//
					// if ($resultname->num_rows > 0){
					// 	while($row1 = $resultname->fetch_assoc()) {
					// 		if($row1['username']==$_SESSION['username'])
					// 		 $userid=$row1["id"];
					// 		 break;
					// 	}
					// }

					if(empty($msg)){
						// insert_words($fp);
						$id_list=insert_new_list($link,  $_SESSION['globaluserid'],$listname);
						insert_words($fp,$id_list,$link);
					  header("location: /linguisi/userlists.php");
					}
					else{
						echo $msg;
					}
					fclose($fp);
				} else{echo 'nie udalo sie otworzyc pliku';}
				unlink($filename);
			}else{echo 'Blad! Akceptowane sa tylko pliki .txt';}
		}
	}

?>


</div>;

</body>

</html>
