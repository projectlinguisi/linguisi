<?php
	function validate_file($fp, $minrecords){
		$msg = "";
		$nrecords = 0;
		$regex = '/^[A-Z|a-z]{1,100}\s[A-Z|a-z]{1,100}$/';
		while($line = fgets($fp)){
			$line = trim($line);
			$match = preg_match($regex,$line);
			if(!$match){
				$msg = "Blad! Plik ma nieprawidlowy format";
				break;
			}
			$nrecords+=1;
		}
		if($nrecords < $minrecords && empty($msg)){
			$msg = "Blad! Plik powinien zawierac co najmniej $minrecords rekordow";
		}
		fseek($fp,0);
		return $msg;
	}
?>