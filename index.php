<?php
  session_start();

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Linguisi</title>
	<link rel="stylesheet" type="text/css" href="style.css">
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">

  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="#"><img src="logo.png" width="200" alt=""></a>
      </nav>
       <h1 class='text-white'>Main panel</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <!-- <li ><a class="btn btn-primary" href="onlinelists.php">Online lists</a></li> -->
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br>
  <br>
  <div class="content w-75 p-3" style="width: 40%; margin: 0px auto;">
      	<p><h3>Welcome <strong><?php echo $_SESSION['username']; ?></strong></h3></p><br><br>

        <a href="games.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Quiz</a><br><br>
        <a href="game1/index.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Words translation</a><br><br>
        <a href="onlinelists.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Online lists</a><br><br>
        <a href="learningplan.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Learning plan</a><br><br>
        <a href="languagegrammar.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Language grammar</a><br><br>
        <a href="statistics.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Statistics</a><br><br>
        <a href="ranking.php" class='btn btn-primary ' style="width: 100%; padding: 6%;">Ranking</a><br><br><br>
  </div>

</body>
</html>
