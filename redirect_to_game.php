<?php
  require_once('test_list.php');
  session_start();

  if (!isset($_SESSION['username'])) {
  	$_SESSION['msg'] = "You must log in first";
  	header('location: login.php');
  }
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }

$_SESSION['err'] = '';
if(isset($_POST['choosen_game']) && isset($_POST["selected_list_id"]) && isset($_POST['nquestions'])){
	$choosen_game = $_POST['choosen_game'];
	$selected_list_id = $_POST['selected_list_id'];
	$nquestions = $_POST['nquestions'];
	$_SESSION['choosen_game'] = $choosen_game;
	$_SESSION['selected_list_id'] = $selected_list_id;
	$_SESSION['nquestions'] = $nquestions;
	if($choosen_game == 'Quiz ABCD'){
		if(!test_list($nquestions, $selected_list_id)){
			$_SESSION['err'] = $_SESSION['err'].'Lista zawiera niewystarczającą liczbę pytań<br>';
			header('location: games.php');
		}
		else{header('location: Quiz/index.php');}
	}
	else{header('location: game1/index.php');}
	//echo 'choosen_game = '.$choosen_game.' idlist = '.$selected_list_id;

}else{
	if(!isset($_POST["selected_list_id"])){
		$_SESSION['err'] = $_SESSION['err'].'Proszę wybrać listę<br>';
	}
	if(!isset($_POST["nquestions"])){
		$_SESSION['err'] = $_SESSION['err'].'Proszę wybrać liczbę pytań';
	}
	header('location: games.php');
}
?>
