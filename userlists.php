<?php
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
  require_once "config.php";

  $uid=$_SESSION['globaluserid'];
  $sql = "SELECT * FROM listlist WHERE userid=$uid";

  $result = $link->query($sql);
}else {
    	header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="index.php"><img src="logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>User lists</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>

  <?php
  if ($result->num_rows > 0) {
          echo "<br>";
          echo "<div class='w-75 p-3' style='width: 40%; margin: 0px auto;'>";
          echo "<h1 class='text-white'>User lists</h1>";
          // echo "<div class='row justify-content-center'>";
          // echo "<div class='col-auto'>";
          // echo "<a href='index.php' class='btn btn-primary'>Back</a>";

          echo "<a href='create.php' class='btn btn-primary'>Create New list</a>";
          echo "<a href='DictionaryUpload/index.php' class='btn btn-primary'>Create list from file</a>";
          echo "<br>";
          echo "<br>";
          echo "<table border='1' class='table table-striped table-dark'>";
          echo "<thead>";
          echo "<tr class='text-white'>";
          echo "<th>ID</th>";
          echo "<th>Name</th>";
          echo "<th>Action</th>";
          echo "</tr>";
          echo "</thead>";
          echo "<tbody>";
          while ($row = $result->fetch_assoc()) {
              echo "<tr class='text-white'>";
              echo "<td>" . $row['idlist'] . "</td>";
              echo "<td>" . $row['listname'] . "</td>";
              echo "<td>";
              echo "<a href='read.php?idlist=" . $row['idlist'] . "' class='btn btn-primary'>Read</a>";
              echo "<a href='update.php?idlist=" . $row['idlist'] . "' class='btn btn-info'>Update</a>";
              echo "<a href='delete.php?idlist=" . $row['idlist'] . "' class='btn btn-danger'>Delete</a>";
              echo "</td>";
              echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";
          // Free result set
// echo "</div>";
//           echo "</div>";
          echo "</div>";
          $result->free();
      } else {
          // echo "<a href='index.php' class='btn btn-primary'>Back</a>";
          echo "<br>";
          echo "<div class='w-75 p-3' style='width: 40%; margin: 0px auto;'>";
          echo "<h1 class='text-white'>User lists</h1>";
          echo "<a href='create.php' class='btn btn-primary'>Create list</a>";
          echo "<a href='DictionaryUpload/index.php' class='btn btn-primary'>Create list from file</a>";
          echo "<p class='lead'><em>No records were found.</em></p>";
          echo "</div>";
      }
  $link->close();
  ?>

</body>
</html>
