<?php
	if (!defined('DB_SERVER'))
		define('DB_SERVER', 'localhost:3307');
	if (!defined('DB_USERNAME'))
		define('DB_USERNAME', 'root');
	if (!defined('DB_PASSWORD'))
		define('DB_PASSWORD', '');
	if (!defined('DB_NAME'))
		define('DB_NAME', 'linguisidb');

	if (!defined('GAME_NUMBER_OF_DRAWS'))
		define('GAME_NUMBER_OF_DRAWS', 3);
