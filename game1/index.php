<?php
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: ../login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <style>
        .btn{
            margin-left: 10px;
        }
    </style>
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('../bgs/linguisi_background_dark.jpg');" >
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="../index.php"><img src="../logo.png" width="200" alt=""></a>
      </nav>
         <h1 class='text-white'>Word translation</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="../index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <br>
	<div id="application" class="content w-75 p-3 text-white" style="width: 100%; margin: 0px auto; background-color: #2952a3;">
		<?php
			if (!isset($_SESSION['username']))
				header("Location: ../login.php");

			require_once __DIR__.'/Router.php';
			Router::Route();
		?>

  </div>


</body>
</html>
