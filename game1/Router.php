<?php

require_once __DIR__.'/Controller/GameController.php';
require_once __DIR__.'/Controller/ListController.php';

class Router
{
	/**
	Przekierowuje adres URL do odpowiedniego kontrolera wg metody
	Przekierowuje domyślnie do widoku 404 jeśli ścieżka jest nieprawidłowa
	*/
	public static function Route()
	{	
		$method = $_SERVER['REQUEST_METHOD'];
		$uri = strtok($_SERVER['REQUEST_URI'], '?');
		
		$path = '/'.implode('/', array_slice(explode('/', $uri), 3));
		
		if ($method == 'GET')
		{
			if ($path == '/' || $path == '/index' || $path == '/index.php')
			{
				return GameController::RenderGet();
			}
			else if ($path == '/list' || $path == '/list.php')
			{
				return ListController::RenderGet();
			}
		}
		else if ($method == 'POST')
		{
			if ($path == '/' || $path == '/index' || $path == '/index.php')
			{
				return GameController::RenderPost();
			}
			else if ($path == '/list' || $path == '/list.php')
			{
				return ListController::RenderPost();
			}
		}
		
		require_once __DIR__.'/View/404.php';
	}
	/**
	Wydruk zapytania do serwera wraz z argumentami GET i POST
	*/
	public static function PrintRequest()
	{
		print_r($_SERVER['REQUEST_METHOD']);
		print(' : ');
		print_r($_SERVER['REQUEST_URI']);
		print('<br>GET: ');
		print_r($_GET);
		print('<br>POST: ');
		print_r($_POST);
		print('<br>');
	}
}
