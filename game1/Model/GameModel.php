<?php

require_once __DIR__.'/../GameConfig.php';

class GameModel
{
	/**
	Drukuje stan gry
	*/
	private static function PrintGameState()
	{
		print('Offset:');
		echo '<br>';
		print_r(GameModel::GetOffset());
		echo '<br>';
		print('Drawn Words:');
		echo '<br>';
		print_r(GameModel::GetDrawnWords());
		echo '<br>';
		print('Translations:');
		echo '<br>';
		print_r(GameModel::GetTranslations());
		echo '<br>';
		print('Typed Words:');
		echo '<br>';
		print_r(GameModel::GetTypedWords());
		echo '<br>';
		print('Current List Id:');
		echo '<br>';
		print_r(GameModel::GetCurrentGameListId());
		echo '<br>';
	}
	/**
	Zwraca offset do tablicy losowanych słów, tłumaczeń i wpisanych słów
	
	@throws Exception
	@returns int
	*/
	public static function GetOffset()
	{
		if (!isset($_SESSION['offset']))
			throw new Exception("Offset not defined");
		
		return $_SESSION['offset'];
	}

	/**
	Ustawia offset do tablicy losowanych słów, tłumaczeń i wpisanych słów
	
	@param $value - wartość offsetu
	*/
	public static function SetOffset($value)
	{
		$_SESSION['offset'] = $value;
	}

	/**
	Przerabia offset na rundę gry
	
	@returns int
	*/
	public static function GetRound()
	{
		return GameModel::GetOffset() + 1;
	}
	
	/**
	Zwraca tablicę wylosowanych słów
	
	@throws Exception
	@returns string[]
	*/
	public static function GetDrawnWords()
	{
		if (!isset($_SESSION['drawn_words']))
			throw new Exception("Drawn words not defined");
		
		return $_SESSION['drawn_words'];
	}

	/**
	Zwraca element z tablicy wylosowanych słów
	
	@param $index - Index tablicy
	@throws Exception
	@throws OutOfBoundsException
	@returns string
	*/
	public static function GetNthDrawnWord($index)
	{
		if (!isset($_SESSION['drawn_words']))
			throw new Exception("Drawn words not defined");
		if ($index < 0 || $index > count($_SESSION['drawn_words']))
			throw new OutOfBoundsException("Index out of bounds");
		
		return $_SESSION['drawn_words'][$index];
	}

	/**
	Ustawia element pod konkretny index tablicy wylosowanych słów
	
	@param $index - Index tablicy
	@param $value - Wartość wstawianego słowa
	@throws Exception
	*/
	public static function SetNthDrawnWord($index, $value)
	{
		if (!isset($_SESSION['drawn_words']))
			throw new Exception("Drawn words not defined");
		
		$_SESSION['drawn_words'][$index] = $value;
	}

	/**
	Resetuje tablicę wylosowanych słów
	*/
	public static function ResetDrawnWords()
	{
		$_SESSION['drawn_words'] = [];
	}

	/**
	Zwraca tablicę napisanych słów
	
	@throws Exception
	@returns string []
	*/
	public static function GetTypedWords()
	{
		if (!isset($_SESSION['typed_words']))
			throw new Exception("Typed words not defined");
		
		return $_SESSION['typed_words'];
	}

	/**
	Zwraca słowo o danym indeksie z tablicy wpisanych słów
	
	@throws Exception
	@throws OutOfBoundsException
	@returns string
	*/
	public static function GetNthTypedWord($index)
	{
		if (!isset($_SESSION['typed_words']))
			throw new Exception("Typed words not defined");
		if ($index < 0 || $index > count($_SESSION['typed_words']))
			throw new OutOfBoundsException("Index out of bounds");
		
		return $_SESSION['typed_words'][$index];
	}
	
	/**
	Wstawa słowo na dany index do tablicy wpisanych słów
	
	@throws Exception
	*/
	public static function SetNthTypedWord($index, $value)
	{
		if (!isset($_SESSION['typed_words']))
			throw new Exception("Typed words not defined");
		
		$_SESSION['typed_words'][$index] = $value;
	}

	/**
	Resetuje tablicę wpisanych słów
	*/
	public static function ResetTypedWords()
	{
		$_SESSION['typed_words'] = [];
	}

	/**
	Zwraca tablicę przetłumaczonych słów
	
	@throws Exception
	@return string[]
	*/
	public static function GetTranslations()
	{
		if (!isset($_SESSION['translations']))
			throw new Exception("Translations not defined");
		
		return $_SESSION['translations'];
	}

	/**
	Zwraca słowo o danym indeksie z tablicy przetłumaczonych słów
	
	@throws Exception
	@throws OutOfBoundsException
	@returns string
	*/
	public static function GetNthTranslation($index)
	{
		if (!isset($_SESSION['translations']))
			throw new Exception("Translations not defined");
		if ($index < 0 || $index > count($_SESSION['translations']))
			throw new OutOfBoundsException("Index out of bounds");
		
		return $_SESSION['translations'][$index];
	}

	/**
	Wstawa słowo na dany index do tablicy przetłumaczonych słów
	
	@throws Exception
	*/
	public static function SetNthTranslation($index, $value)
	{
		if (!isset($_SESSION['translations']))
			throw new Exception("Translations not defined");
		
		$_SESSION['translations'][$index] = $value;
	}

	/**
	Resetuje tablicę przetłumaczonych słów
	*/
	public static function ResetTranslations()
	{
		$_SESSION['translations'] = [];
	}

	/**
	Ustawia długość tablicy wylosowanych słów
	*/
	public static function SetNumberOfDraws($value)
	{
		$_SESSION['number_of_draws'] = $value;
	}

	
	/**
	Zwraca długość tablicy wylosowanych słów
	
	@throws Exception
	@returns int
	*/
	public static function GetNumberOfDraws()
	{
		if (!isset($_SESSION['number_of_draws']))
			throw new Exception("Numbef of draws not defined");
		
		return $_SESSION['number_of_draws'];
	}
	
	/**
	Zwraca identyfikator aktualnie używanej listy
	
	@throws Exception
	@returns int
	*/
	public static function GetCurrentGameListId()
	{
		if (!isset($_SESSION['list_id']))
			throw new Exception("List id not defined");
		
		return $_SESSION['list_id'];
	}
	
	/**
	Ustawia id listy aktualnie używanej
	
	@param $value - Identyfikator listy
	*/
	public static function SetCurrentGameListId($value)
	{
		$_SESSION['list_id'] = $value;
	}
	
	/**
	Wyrzuca identyfikator aktualnie używanej listy gry
	*/
	public static function UnsetCurrentGameListId()
	{
		$_SESSION['list_id'] = null;
		unset($_SESSION['list_id']);
	}
}
