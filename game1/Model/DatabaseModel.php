<?php

require_once __DIR__.'/GameModel.php';
require_once __DIR__.'/../GameConfig.php';

class DatabaseModel
{
	/**
	Ładowanie słów do tłumaczenia z konretnej listy.

	@param $listid - Identyfikator listy
	@throws Exception
	*/
	public static function SampleWords($listid)
	{
		if ($listid < 1)
			throw new Exception("List ID must be a valid number. Called with {$listid}");

		$count = 0;
		$draws = GAME_NUMBER_OF_DRAWS;

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed: {$conn->connect_error}");
		}

		# TODO: Filter lists that are empty

		$prepared_statement = $conn->prepare("SELECT word, translation FROM globallist WHERE listid = ? ORDER BY RAND() LIMIT ?");
		$prepared_statement->bind_param("ii", $listid, $draws);
		$prepared_statement->execute();

		$result = $prepared_statement->get_result();
		$result_count = $result->num_rows;
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				array_push($_SESSION['drawn_words'], trim($row['word']));
				array_push($_SESSION['translations'], trim($row['translation']));
			}
		}

		if ($result_count < $draws)
		{
			$count = $result_count;
			GameModel::SetNumberOfDraws($count);
		}
		else
		{
			$count = $draws;
			GameModel::SetNumberOfDraws($count);
		}

		$prepared_statement->close();
		$conn->close();

		return $count;
	}


	/**
	Pobieranie list globalnych i lokalnych danego użytkownika

	@param $userid - Identyfikator użytkownika
	@throws Exception
	*/
	public static function GetLists($userid)
	{
		if ($userid < 1)
			throw new Exception("User ID must be a valid number. Called with {$userid}");

		$global_list_user_id = 1;
		$results = array();

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		$prepared_statement = $conn->prepare("SELECT idlist, userid, listname FROM listlist WHERE userid = ? or userid = ?");
		$prepared_statement->bind_param("ii", $userid, $global_list_user_id);
		$prepared_statement->execute();

		$result = $prepared_statement->get_result();
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$value = [];
				$value['listname'] = $row['listname'];
				$value['userid'] = $row['userid'];
				$results[$row['idlist']] = $value;
			}
		}

		$prepared_statement->close();
		$conn->close();

		return $results;
	}

	/**
	Pobieranie identyfikatora zalogowanego użytkownika

	@throws Exception
	*/
	public static function GetCurrentUserId()
	{
		$id = 0;

		if (empty($_SESSION['username']))
			throw new Exception("Username cannot be an empty value.");

		$username = $_SESSION['username'];

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		$prepared_statement = $conn->prepare("SELECT id FROM users WHERE username = ?");
		$prepared_statement->bind_param("s", $username);
		$prepared_statement->execute();

		$result = $prepared_statement->get_result();
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$id = $row['id'];
		}

		$prepared_statement->close();
		$conn->close();

		return $id;
	}

	/**
	Zapis wyniku gry do globalnego rankingu

	@param $points - Liczba punktów do zapisania
	@throws Exception
	*/
	public static function SaveSummary($points)
	{
		if ($points < 0)
			throw new Exception("Points must be a non-negative number. Called with {$points}");

		$iduser = DatabaseModel::GetCurrentUserId();
		$username = $_SESSION['username'];
		$game = 1;

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		# Get

		$prepared_statement = $conn->prepare("SELECT points FROM rankings WHERE username = ?");
		$prepared_statement->bind_param("s", $username);
		$prepared_statement->execute();

		$current_points = -1;

		$result = $prepared_statement->get_result();
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$current_points = $row['points'];
		}

		$prepared_statement->close();

		if ($current_points == -1) # Insert
		{
			$prepared_statement = $conn->prepare("INSERT INTO rankings(iduser, username, game, points) VALUES (?, ?, ?, ?)");
			$prepared_statement->bind_param("isii", $iduser, $username, $game, $points);
			$prepared_statement->execute();

			$prepared_statement->close();
		}
		else # Update
		{
			$new_points = $current_points + $points;

			$prepared_statement = $conn->prepare("UPDATE rankings SET points = ? WHERE iduser = ?");
			$prepared_statement->bind_param("is", $new_points, $iduser);
			$prepared_statement->execute();

			$prepared_statement->close();
		}

		$conn->close();
	}

	/**
	Zapis wyniku gry do loklanych statystyk użytkownika

	@param $points - Liczba punktów do zapisania
	@throws Exception
	*/
	public static function SaveStatistics($points)
	{
		if ($points < 0)
			throw new Exception("Points must be a non-negative number. Called with {$points}");

		$userid = DatabaseModel::GetCurrentUserId();
		$username = $_SESSION['username'];
		$game = 1;
		$activitydate = date("Y/m/d h:i:s");

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		$prepared_statement = $conn->prepare("INSERT INTO stats(userid, username, game, points, activitydate) VALUES (?, ?, ?, ?, ?)");
		$prepared_statement->bind_param("isiis", $userid, $username, $game, $points, $activitydate);
		$prepared_statement->execute();

		$prepared_statement->close();
		$conn->close();
	}

	/**
	Testuje czy lista jest globalna

	@param $list_id - Identyfikator listy
	@throws Exception
	*/
	public static function IsGlobalList($list_id)
	{
		$userid = 0;

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		$prepared_statement = $conn->prepare("SELECT userid FROM listlist WHERE idlist = ?");
		$prepared_statement->bind_param("i", $list_id);
		$prepared_statement->execute();

		$result = $prepared_statement->get_result();
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$userid = $row['userid'];
		}

		$prepared_statement->close();
		$conn->close();

		if ($userid == 1)
			return true;
		return false;
	}

	/**
	Testuje czy lista jest lokalna

	@param $list_id - Identyfikator listy
	@throws Exception
	*/
	public static function IsLocalList($list_id)
	{
		$userid = 0;

		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
		if ($conn->connect_error) {
			throw new Exception("Connection failed {$conn->connect_error}: ");
		}

		$prepared_statement = $conn->prepare("SELECT userid FROM listlist WHERE idlist = ?");
		$prepared_statement->bind_param("i", $list_id);
		$prepared_statement->execute();

		$result = $prepared_statement->get_result();
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$userid = $row['userid'];
		}

		$prepared_statement->close();
		$conn->close();

		if ($userid != 1 && $userid != 0)
			return true;
		return false;
	}
}
