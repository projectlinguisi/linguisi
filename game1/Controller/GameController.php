<?php

require_once __DIR__.'/../GameConfig.php';
require_once __DIR__.'/../Model/GameModel.php';
require_once __DIR__.'/../Model/DatabaseModel.php';

class GameController
{
	/**
	Pokazanie widoku aplikacji przy zapytaniu przez metode GET
	*/
	public static function RenderGet()
	{
		GameController::AssureSessionInit();
		GameController::AssureDrawnWords();
		GameController::AssureTranslations();
		GameController::AssureCurrentListId();

		$view = [];

		if (GameModel::GetOffset() < GameModel::GetNumberOfDraws())
		{
			GameController::SetTranslateViewVariables($view);
			require_once __DIR__.'/../View/Translate.php';
		}
		else
		{
			GameController::SetSummaryViewVariables($view);
			require_once __DIR__.'/../View/Summary.php';
			
			$points = $view['good_answers_count'];
			
			if (GameModel::GetNumberOfDraws() != 0)
			{
				$list_id = $_SESSION['list_id'];
				
				if (DatabaseModel::IsGlobalList($list_id))
					DatabaseModel::SaveSummary($points);
				
				DatabaseModel::SaveStatistics($points);
			}
			
			GameModel::UnsetCurrentGameListId();
		}
	}

	/**
	Pokazanie widoku aplikacji przy zapytaniu przez metode POST
	*/
	public static function RenderPost()
	{
		if (!isset($_POST['list']) && !isset($_SESSION['list_id']))
		{
			GameController::ResetGameState();
			header("Location: list");
			exit;
		}
		else if (isset($_POST['list']) && !isset($_SESSION['list_id']))
		{
			$list_id = 0;
			foreach ($_POST['list'] as $key => $value)
			{
				$list_id = $key;
				break;
			}
			
			GameController::ResetGameState();
			GameModel::SetCurrentGameListId($list_id);
			GameController::LoadWordsFromDatabase();
		}

		if (isset($_POST['translation']))
		{
			GameController::BumpRound();
		}
		
		header('Location: index');
	}

	/**
	Ustawienie parametrów dla widoku Translate
	
	@param $view - nadpisywana zmienna widoku
	*/
	public static function SetTranslateViewVariables(&$view)
	{
		$view['round'] = GameModel::GetRound();
		$view['translation'] = GameModel::GetNthTranslation(GameModel::GetOffset());
		$view['word'] = GameModel::GetNthDrawnWord(GameModel::GetOffset());
		$view['number_of_draws'] = GameModel::GetNumberOfDraws();
	}

	/**
	Ustawienie parametrów dla widoku Summary
	
	@param $view - nadpisywana zmienna widoku
	*/
	public static function SetSummaryViewVariables(&$view)
	{
		$drawn_words = GameModel::GetDrawnWords();
		$translations = GameModel::GetTranslations();
		$typed_words = GameModel::GetTypedWords();
		$number_of_draws = GameModel::GetNumberOfDraws();
		
		$good_answers_count = 0;
		$good_answers_indices = [];
		$bad_answers_count = 0;
		$bad_answers_indices = [];
		$correctness = 0;

		for ($i = 0; $i < $number_of_draws; ++$i)
		{
			if (strcmp($translations[$i], $typed_words[$i]) == 0)
			{
				$good_answers_count = $good_answers_count + 1;
				array_push($good_answers_indices, $i);
			}
			else
			{
				$bad_answers_count = $bad_answers_count + 1;
				array_push($bad_answers_indices, $i);
			}
		}
		
		$view['listid'] = GameModel::GetCurrentGameListId();
		$view['drawn_words'] = $drawn_words;
		$view['translations'] = $translations;
		$view['typed_words'] = $typed_words;
		$view['number_of_draws'] = $number_of_draws;
		$view['good_answers_count'] = $good_answers_count;
		$view['good_answers_indices'] = $good_answers_indices;
		$view['bad_answers_count'] = $bad_answers_count;
		$view['bad_answers_indices'] = $bad_answers_indices;
		if ($number_of_draws == 0)
			$view['correctness'] = 0;
		else
			$view['correctness'] = $good_answers_count / $number_of_draws;
	}
	
	/**
	Zapewnia, że zmienna sesji jest ustawiona, w przeciwnym razie resetuje stan gry i przekierwuje do wyboru listy.	
	*/
	
	private static function AssureSessionInit()
	{
		if (empty($_SESSION['init']))
		{
			$_SESSION['init'] = true;
			GameController::ResetGameState();
			header("Location: list");
			exit;
		}
	}
	/**
	Zapewnia, że zmienna gry wylosowanych słów jest ustawiona, w przeciwnym razie resetuje stan gry i przekierwuje do wyboru listy.	
	*/
	private static function AssureDrawnWords()
	{
		if (!isset($_SESSION['drawn_words']))
		{
			GameController::ResetGameState();
			header("Location: list");
			exit;
		}
	}
	
	/**
	Zapewnia, że zmienna gry tłumaczeń jest ustawiona, w przeciwnym razie resetuje stan gry i przekierwuje do wyboru listy.	
	*/
	private static function AssureTranslations()
	{
		if (!isset($_SESSION['translations']))
		{
			GameController::ResetGameState();
			header("Location: list");
			exit;
		}
	}
	
	/**
	Zapewnia, że zmienna gry aktualnej listy słów jest ustawiona, w przeciwnym razie resetuje stan gry i przekierwuje do wyboru listy.	
	*/
	private static function AssureCurrentListId()
	{
		if (!isset($_SESSION['list_id']))
		{
			GameController::ResetGameState();
			header("Location: list");
			exit;
		}
	}

	/**
	Resetuje stan gry
	*/
	public static function ResetGameState()
	{
		GameModel::SetOffset(0);
		GameModel::ResetDrawnWords();
		GameModel::ResetTranslations();
		GameModel::ResetTypedWords();
		GameModel::UnsetCurrentGameListId();
	}

	/**
	Podbija rundę gry
	*/
	private static function BumpRound()
	{
		GameController::StoreTypedWord();

		GameModel::SetOffset(GameModel::GetOffset() + 1);
	}

	/**
	Zapisuje kolejne wpisane tłumaczenie odpowiednie do rundy gry
	*/
	private static function StoreTypedWord()
	{
		GameModel::SetNthTypedWord(GameModel::GetOffset(), $_POST['translation']);
	}

	/**
	Ładuje słowa z bazy danych
	*/
	private static function LoadWordsFromDatabase()
	{
		$count = 0;
		$listid = GameModel::GetCurrentGameListId();
		
		try {
			$count = DatabaseModel::SampleWords($listid);
		} catch (Exception $e) {
			GameController::ResetGameState();
			die($e->getMessage());
		}
	}
}
