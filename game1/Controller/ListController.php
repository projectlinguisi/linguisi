<?php

require_once __DIR__.'/../GameConfig.php';
require_once __DIR__.'/../Model/GameModel.php';
require_once __DIR__.'/../Controller/GameController.php';

class ListController
{
	/**
	Pokazanie widoku aplikacji przy zapytaniu przez metode GET
	*/
	public static function RenderGet()
	{
		if (empty($_SESSION['init']))
			header('Location: index');
		
		$view = [];
		ListController::SetSelectListViewVariables($view);
		require_once __DIR__.'/../View/SelectList.php';
	}
	
	/**
	Ustawienie parametrów dla widoku list
	
	@param $view - nadpisywana zmienna widoku
	*/
	public static function SetSelectListViewVariables(&$view)
	{
		$view['global_list_user_id'] = 1;
		$view['lists'] = DatabaseModel::GetLists(DatabaseModel::GetCurrentUserId());
	}
	/**
	Pokazanie widoku aplikacji przy zapytaniu przez metode POST
	*/
	public static function RenderPost()
	{
		if (empty($_SESSION['init']))
			header('Location: index');
		
		GameController::ResetGameState();
		header("Location: list");
	}
}
