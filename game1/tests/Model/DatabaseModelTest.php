<?php
declare(strict_types=1);

require_once __DIR__.'/../../Model/DatabaseModel.php';

use PHPUnit\Framework\TestCase;

final class DatabaseModelTest extends TestCase
{
	private function RunSession()
	{
		if (!isset($_SESSION))
		{
		 // If we are run from the command line interface then we do not care
		 // about headers sent using the session_start.
		 if (PHP_SAPI === 'cli')
		 {
			global $_SESSION;
			$_SESSION = array();
		 }
		 elseif (!headers_sent())
		 {
			if (!session_start())
			{
			   throw new Exception(__METHOD__ . 'session_start failed.');
			}
		 }
		 else
		 {
			throw new Exception(
			   __METHOD__ . 'Session started after headers sent.');
		 }
		}
		else
			$_SESSION = [];
	}
	
	public function testSampleWordsThrows()
	{
		$this->expectException(Exception::class);
		DatabaseModel::SampleWords(0);
	}
	
	public function testGetListsThrows()
	{
		$this->expectException(Exception::class);
		DatabaseModel::GetLists(0);
	}
	
	public function testGetCurrentUserIdThrows()
	{
		$this->RunSession();
		
		$this->expectException(Exception::class);
		DatabaseModel::GetCurrentUserId();
	}
	
	public function testSaveSummaryThrows()
	{
		$this->expectException(Exception::class);
		DatabaseModel::SaveSummary(-1);
	}
	
	public function testSaveStatisticsThrows()
	{
		$this->expectException(Exception::class);
		DatabaseModel::SaveStatistics(-1);
	}
	
	
}
?>
