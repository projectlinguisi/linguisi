<?php
declare(strict_types=1);

require_once __DIR__.'/../../Model/GameModel.php';

use PHPUnit\Framework\TestCase;

final class GameModelTest extends TestCase
{
	private function RunSession()
	{
		if (!isset($_SESSION))
		{
		 // If we are run from the command line interface then we do not care
		 // about headers sent using the session_start.
		 if (PHP_SAPI === 'cli')
		 {
			global $_SESSION;
			$_SESSION = array();
		 }
		 elseif (!headers_sent())
		 {
			if (!session_start())
			{
			   throw new Exception(__METHOD__ . 'session_start failed.');
			}
		 }
		 else
		 {
			throw new Exception(
			   __METHOD__ . 'Session started after headers sent.');
		 }
		}
		else
			$_SESSION = [];
	}
	
	public function testGetOffset()
	{
		$this->RunSession();
		
		$_SESSION['offset'] = 0;
		$this->assertEquals(GameModel::GetOffset(), 0);
	}
	
	public function testGetOffsetThrows()
	{
		$this->RunSession();
		
		$this->expectException(Exception::class);
		GameModel::GetOffset();
	}
	
	public function testSetOffset()
	{
		$this->RunSession();
		
		$_SESSION['offset'] = 0;
		GameModel::SetOffset(15);
		$this->assertEquals($_SESSION['offset'], 15);
		$this->assertNotEquals($_SESSION['offset'], 0);
	}

	public function testGetRound()
	{
		$this->RunSession();
		$_SESSION['offset'] = 0;
		$this->assertEquals(GameModel::GetRound(), 1);
	}
	
	public function testGetDrawnWords()
	{
		$this->RunSession();
		
		$_SESSION['drawn_words'] = 0;
		$this->assertEquals(GameModel::GetDrawnWords(), 0);
	}

	public function testGetDrawnWordsThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::GetDrawnWords();
		
	}

	public function testGetNthDrawnWord()
	{
		$this->RunSession();
		
		$_SESSION['drawn_words'] = ['test'];
		$this->assertEquals(GameModel::GetNthDrawnWord(0), 'test');
		
	}
	
	public function testGetNthDrawnWordThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::GetNthDrawnWord(0);
	}
	
	public function testGetNthDrawnWordThrowsIndex()
	{
		$this->RunSession();
		$this->expectException(OutOfBoundsException::class);
		$_SESSION['drawn_words'] = ['test'];
		GameModel::GetNthDrawnWord(2);
	}

	public function testSetNthDrawnWord()
	{
		$this->RunSession();
		$_SESSION['drawn_words'] = ['test'];
		GameModel::SetNthDrawnWord(0, "test2");
		$this->assertEquals($_SESSION['drawn_words'][0], 'test2');
		
		GameModel::SetNthDrawnWord(1, "test3");
		$this->assertEquals($_SESSION["drawn_words"][1], "test3");
	}
	
	public function testSetNthDrawnWordThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::SetNthDrawnWord(0, "test");
	}

	public function testResetDrawnWords()
	{
		$this->RunSession();
		$_SESSION['drawn_words'] = ['test'];
		GameModel::ResetDrawnWords();
		$this->assertEmpty($_SESSION['drawn_words']);
	}
	
	public function testGetTypedWords()
	{
		$this->RunSession();
		
		$_SESSION['typed_words'] = ['test'];
		$this->assertEquals(GameModel::GetTypedWords()[0], 'test');
	}
	
	public function testGetTypedWordsThrows()
	{
		$this->RunSession();
		
		$this->expectException(Exception::class);
		GameModel::GetTypedWords();
	}
	
	public function testGetNthTypedWord()
	{
		$this->RunSession();
		
		$_SESSION['typed_words'] = ['test'];
		$this->assertEquals(GameModel::GetNthTypedWord(0), 'test');
		
	}
	
	public function testGetNthTypedWordThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::GetNthTypedWord(0);
	}
	
	public function testGetNthTypedWordThrowsIndex()
	{
		$this->RunSession();
		$this->expectException(OutOfBoundsException::class);
		$_SESSION['typed_words'] = ['test'];
		GameModel::GetNthTypedWord(2);
	}

	public function testSetNthTypedWord()
	{
		$this->RunSession();
		$_SESSION['typed_words'] = ['test'];
		GameModel::SetNthTypedWord(0, "test2");
		$this->assertEquals($_SESSION['typed_words'][0], 'test2');
		
		GameModel::SetNthTypedWord(1, "test3");
		$this->assertEquals($_SESSION["typed_words"][1], "test3");
	}
	
	public function testSetNthTypedWordThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::SetNthTypedWord(0, "test");
	}

	public function testResetTypedWords()
	{
		$this->RunSession();
		$_SESSION['typed_words'] = ['test'];
		GameModel::ResetTypedWords();
		$this->assertEmpty($_SESSION['typed_words']);
	}

	public function testGetTranslations()
	{
		$this->RunSession();
		
		$_SESSION['translations'] = ['test'];
		$this->assertEquals(GameModel::GetTranslations()[0], 'test');
	}
	
	public function testGetTranslationsThrows()
	{
		$this->RunSession();
		
		$this->expectException(Exception::class);
		GameModel::GetTranslations();
	}

	public function testGetNthTranslation()
	{
		$this->RunSession();
		
		$_SESSION['translations'] = ['test'];
		$this->assertEquals(GameModel::GetNthTranslation(0), 'test');
		
	}
	
	public function testGetNthTranslationThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::GetNthTranslation(0);
	}
	
	public function testGetNthTranslationThrowsIndex()
	{
		$this->RunSession();
		$this->expectException(OutOfBoundsException::class);
		$_SESSION['translations'] = ['test'];
		GameModel::GetNthTranslation(2);
	}

	public function testSetNthTranslation()
	{
		$this->RunSession();
		$_SESSION['translations'] = ['test'];
		GameModel::SetNthTranslation(0, "test2");
		$this->assertEquals($_SESSION['translations'][0], 'test2');
		
		GameModel::SetNthTranslation(1, "test3");
		$this->assertEquals($_SESSION["translations"][1], "test3");
	}
	
	public function testSetNthTranslationThrows()
	{
		$this->RunSession();
		$this->expectException(Exception::class);
		GameModel::SetNthTranslation(0, "test");
	}

	public function testResetTranslations()
	{
		$this->RunSession();
		$_SESSION['translations'] = ['test'];
		GameModel::ResetTranslations();
		$this->assertEmpty($_SESSION['translations']);
	}

	public function testSetNumberOfDraws()
	{
		$this->RunSession();
		
		$_SESSION['number_of_draws'] = 0;
		GameModel::SetNumberOfDraws(15);
		$this->assertEquals($_SESSION['number_of_draws'], 15);
		$this->assertNotEquals($_SESSION['number_of_draws'], 0);
	}

	public function testGetNumberOfDraws()
	{
		$this->RunSession();
		
		$_SESSION['number_of_draws'] = 0;
		$this->assertEquals(GameModel::GetNumberOfDraws(), 0);
	}
	
	public function testGetNumberOfDrawsThrows()
	{
		$this->RunSession();
		
		$this->expectException(Exception::class);
		GameModel::GetNumberOfDraws();
	}

	public function testGetCurrentGameListId()
	{
		$this->RunSession();
		
		$_SESSION['list_id'] = 0;
		$this->assertEquals(GameModel::GetCurrentGameListId(), 0);
	}
	
	public function testSetCurrentGameListId()
	{
		$this->RunSession();
		
		$_SESSION['list_id'] = 0;
		GameModel::SetCurrentGameListId(15);
		$this->assertEquals($_SESSION['list_id'], 15);
		$this->assertNotEquals($_SESSION['list_id'], 0);
	}
}