<?php if ($view['number_of_draws'] > 0): ?>

<div>
	<h3 class='text-white'>Summary</h3>
	<p>Correct answers: <?= $view['good_answers_count'] ?></p>
	<ul>
		<?php foreach($view['good_answers_indices'] as $index): ?>
		<li><?= $view['drawn_words'][$index] ?> => <?= $view['translations'][$index]?></li>
		<?php endforeach; ?>
	</ul>
	<p>Bad answers: <?= $view['bad_answers_count'] ?></p>
	<ul>
		<?php foreach($view['bad_answers_indices'] as $index): ?>
		<li><?= $view['drawn_words'][$index] ?> => <?= $view['translations'][$index]?>, but you typed <?= $view['typed_words'][$index] ?></li>
		<?php endforeach; ?>
	</ul>
	<p>Correctness: <?= $view['correctness'] * 100 ?> %</p>
</div>

<form action = "index" method = "post">
	<input type = "submit" value = "Next round!"/>
	<input type="hidden" name="list[<?= $view['listid'] ?>]" />
</form>

<form action = "list" method = "post">
	<input type = "submit" value = "Exit game" />
</form>

<?php else: ?>
	
<p>List is empty</p>
<form action = "list" method = "get">
	<input type = "submit" value = "Go back"/>
</form>

<form action = "../" method = "get">
	<input type = "submit" value = "Exit game" />
</form>
	
<?php endif; ?>
