<div>
	<h3>Global Lists:</h3>
	
	<ul>
		<?php foreach($view['lists'] as $key => $value): ?>
		<?php if ($value['userid'] == $view['global_list_user_id']): ?>
		<li>
			<form action = "index" method = "post">
				<input type="submit" name="list[<?= $key ?>]" value="<?= $value['listname'] ?>">
				
			</form>
		</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	
	<h3>Local Lists:</h3>
	
	<ul>
		<?php foreach($view['lists'] as $key => $value): ?>
		<?php if ($value['userid'] != $view['global_list_user_id']): ?>
		<li>
			<form action = "index" method = "post">
				<input type="submit" name="list[<?= $key ?>]" value="<?= $value['listname'] ?>">
				
			</form>
		</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
</div>

<form action = "../" method = "get">
	<input type = "submit" value = "Main menu" />
</form>