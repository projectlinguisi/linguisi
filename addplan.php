<?php
require_once "config.php";
session_start();
if (isset($_GET['logout'])) {
  session_destroy();
  unset($_SESSION['username']);
  header("location: login.php");
}
if (isset($_SESSION['username'])){
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $param_userid=0;

      if (isset($_SESSION['username'])) {

          $sql = "SELECT * FROM users WHERE username = ?";
          if ($stmt = $link->prepare($sql)) {
              $stmt->bind_param("s", $_SESSION["username"]);
              if ($stmt->execute()) {
                  $result = $stmt->get_result();
                  if ($result->num_rows == 1) {
                      $row = $result->fetch_array(MYSQLI_ASSOC);
                      $param_userid = $row["id"];
                  } else {
                      echo "Error! Data Not Found1";
                      header("location: learningplan.php");
                      exit();
                  }
              } else {
                  echo "Error! Please try again later.";
                  header("location: learningplan.php");
                  exit();
              }
              $stmt->close();
          }
      } else {
          echo $param_userid;
          header("location: learningplan.php");
          exit();
      }
        if (isset($_POST['listname'])&&isset($_POST['startdate'])&&isset($_POST['enddate'])) {
            //add check if date is bigger than other
            // header("location: index.php");
            $intnull = null;
            $listname=$_POST['listname'];

            $startdate=htmlentities($_POST['startdate']);
            $enddate=htmlentities($_POST['enddate']);

            $datestart = date("Y-m-d H:i:s", strtotime($startdate));
            $dateend = date("Y-m-d H:i:s", strtotime($enddate));

            $sql = "INSERT INTO plans (id, userid, listname, startDate, endDate) VALUES (?,?,?,?,?)";


            echo "<script>console.log('" . $param_userid . "' );</script>";
            if ($stmt = $link->prepare($sql)) {
              // INSERT INTO plans (userid, listname, startDate, endDate) VALUES (1,'asd','1999-12-12','2000-11-11')
                $stmt->bind_param("iisss", $intnull, $param_userid, $listname, $datestart, $dateend);
                if ($stmt->execute()) {
                    header("location: index.php");
                    exit();
                } else {
                    echo "Error! Please try again later.";
                    header("location: learningplan.php");
                }
                $stmt->close();
            }
        }

        $link->close();
    }
}else {
    	header('location: login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Linguisi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body class="bg-dark" style="height: 1000px; background-image: url('bgs/linguisi_background_dark.jpg');">
  <header>
  <nav class="navbar navbar-expand-md navbar-dark bg-success">
    <div class="container-fluid">
      <nav class="navbar">
        <a class="m-0" href="#"><img src="logo.png" width="200" alt=""></a>
      </nav>
      <h1 class='text-white'>Add plan</h1>
      <!--<a class="navbar-brand" href="#"><img src="logo.png" width="100" alt="" class="mr-5"></a>-->
      <div class="navbar">
        <ul class="navbar-nav">
          <li ><a class="btn btn-danger" href="index.php?logout='1'"> logout </a></li>
        </ul>
      </div>
    </div>
  </nav>
  </header>
  <div class="p-3 mb-2 bg-dark text-white w-75 p-3" style="width: 40%; margin: 0px auto;">
       <h2>Add plan</h2>
    <form action="addplan.php" method="post">
            <label><h5>Plan name</h5></label>
            <input type="text" name="listname" class="form-control" required><br>
            <label><h5>Start date</h5></label>
            <input type="date" name="startdate" required><br>
            <!-- <input type="text" name="startdate" class="form-control" required><br> -->
            <label><h5>End date</h5></label>
            <input type="date" name="enddate" required><br>
            <!-- <input type="text" name="enddate" class="form-control" required><br> -->
            <!-- <button type="submit" class="btn" name="create">Submit</button> -->
        <input type="submit" class="btn btn-primary" value="Submit"><br>

        <a href="learningplan.php" class="btn btn-default">Cancel</a>

    </form>
</div>

</body>
</html>
